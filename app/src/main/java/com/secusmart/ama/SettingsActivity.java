package com.secusmart.ama;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Fullscreen;
import org.androidannotations.annotations.PreferenceChange;
import org.androidannotations.annotations.PreferenceScreen;
import org.androidannotations.annotations.res.StringRes;

@Fullscreen
@PreferenceScreen(R.xml.preferences)
@EActivity
public class SettingsActivity extends PreferenceActivity {

    @StringRes
    String toastTextSaved;

    SharedPreferences prefs;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
    }

    @PreferenceChange(R.string.preferences_auto_mode)
    void preferences_auto_mode(Preference preference) {
        Toast.makeText(this, toastTextSaved, Toast.LENGTH_SHORT).show();
    }

    @PreferenceChange(R.string.preferences_temp)
    void preferences_temp(Preference preference) {
        Toast.makeText(this, toastTextSaved, Toast.LENGTH_SHORT).show();
    }
}
