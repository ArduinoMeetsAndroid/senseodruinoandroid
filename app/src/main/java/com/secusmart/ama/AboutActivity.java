package com.secusmart.ama;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Fullscreen;
import org.androidannotations.annotations.res.IntegerRes;
import org.androidannotations.annotations.res.StringRes;

import java.util.List;

@Fullscreen
@EActivity(R.layout.activity_about)
public class AboutActivity extends AppCompatActivity {
  private final String TAG = this.getClass().getSimpleName();

  @IntegerRes
  Integer vibrationTime;

  @StringRes
  String urlFacebookApp;
  @StringRes
  String urlFacebookBrowser;
  @StringRes
  String urlTwitterBrowser;
  @StringRes
  String urlInstagramBrowser;
  @StringRes
  String urlWebBrowser;

  private Vibrator vibrator;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    vibrator = (Vibrator) this.getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
  }

  public void fb(View view) {
    Log.d(TAG, "facebook");
    vibrator.vibrate(vibrationTime);
    launchFacebook();
  }

  public final void launchFacebook() {
    Intent intent = new Intent(Intent.ACTION_VIEW);
    intent.setData(Uri.parse(urlFacebookBrowser));

    final PackageManager packageManager = getPackageManager();
    List<ResolveInfo> list = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
    if (list.size() == 0) {
      intent.setData(Uri.parse(urlFacebookApp));
    }

    startActivity(intent);
  }

  public void tweet(View view) {
    Log.d(TAG, "Twitter");
    vibrator.vibrate(vibrationTime);
    Intent tweetIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(urlTwitterBrowser));
    startActivity(tweetIntent);
  }

  public void insta(View view) {
    Log.d(TAG, "Instagram");
    vibrator.vibrate(vibrationTime);
    Intent instaIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(urlInstagramBrowser));
    startActivity(instaIntent);
  }

  public void web(View view) {
    Log.d(TAG, "Webside");
    vibrator.vibrate(vibrationTime);
    Intent webIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(urlWebBrowser));
    startActivity(webIntent);
  }
}
